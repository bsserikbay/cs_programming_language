using System;

namespace SimpleException
{
    public class Car
    {
        public const int MaxSpeed = 100;

        public int CurrentSpeed { get; set; } = 0;
        public string PetName { get; set; } = "";
        
        private bool carIsDead;
        
        private Radio theMusicBox = new Radio();

        public Car()
        {
            
        }

        public Car(string name, int speed)
        {
            PetName = name;
            CurrentSpeed = speed;
        }
        //
        public void CrankTunes(bool state)
        {
            theMusicBox.TurnOn(state);
        }

        public void Accelerate(int delta)
        {
            if (carIsDead)
                Console.WriteLine("{0} is out of order", PetName);
            else
            {
                CurrentSpeed += delta;
                if (CurrentSpeed >= MaxSpeed)
                {
                    CurrentSpeed = 0;
                    carIsDead = true;
                    throw new Exception($"{PetName} is overheated");

                }
                Console.WriteLine("=> Current Speed = {0}", CurrentSpeed);
            }
        }
    }
}