﻿using System;

namespace SimpleException
{
    class Program
    {
        static void Main(string[] args)
        {
            var myCar = new Car("Daisy", 20);
            myCar.CrankTunes(true);

            try
            {
                for (int i = 0; i < 10; i++)
                {
                    myCar.Accelerate(10);
                    Console.ReadLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n**** ERROR! *****");
                Console.WriteLine("Stack: {0}", e.StackTrace);
                Console.WriteLine("Method: {0}", e.TargetSite);
                Console.WriteLine("Class defining member: {0}", e.TargetSite.DeclaringType);
                Console.WriteLine("Member type: {0}", e.TargetSite.MemberType);
                Console.WriteLine("Message: {0}", e.Message);
                Console.WriteLine("Source: {0}", e.Source);
            }

            Console.WriteLine("\n*** Out of exception logic ***");
            Console.ReadLine();
        }
    }
}