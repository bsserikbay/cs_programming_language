﻿using System;

namespace Basiclnheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*******   Basic Inheritance  *******");
            
            Car myCar = new Car(80);
            myCar.Speed = 50;

            Console.WriteLine("My car is going {0} MPH", myCar.Speed);
            Console.ReadLine();

            MiniVan mv = new MiniVan();
            mv.Speed = 5;
            Console.WriteLine("Minivan is going {0} MPH", mv.Speed);
            Console.ReadLine();
        }
    }
}