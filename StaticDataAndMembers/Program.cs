﻿using System;

namespace StaticDataAndMembers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Static Data *****\n");
            
            SavingAccount s1 = new SavingAccount(50); 
            
            Console.WriteLine ("Interest Rate is: {0}", SavingAccount.GetInterestRate()) ;
            
            SavingAccount.SetInterestRate(0.008);
            
            SavingAccount s2 = new SavingAccount(100); 
                
            Console.WriteLine("Interest Rate is: {0}", SavingAccount.GetInterestRate()) ;

            SavingAccount s3 = new SavingAccount(10000.75);
                
            

            Console.ReadLine();
        }
    }
}