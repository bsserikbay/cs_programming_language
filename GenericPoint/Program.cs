﻿using System;

namespace GenericPoint
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with generic structures");
            Point<int> p = new Point<int>(10, 10);
            Console.WriteLine("p.ToString()={0}", p.ToString());
            p.ResetPoint();
            Console.WriteLine("p.ToString()={0}", p.ToString());
            Console.WriteLine();

            Point<double> p1 = new Point<double>(5.4, 3.3);
            Console.WriteLine("p1.ToString()={0}", p1.ToString());
            p1.ResetPoint();
            Console.WriteLine("p1.ToString()={0}", p1.ToString());
            Console.WriteLine();
        }
    }
}