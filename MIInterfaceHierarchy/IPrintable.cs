namespace MIInterfaceHierarchy
{
    public interface IPrintable
    {
        void Print();
        void Draw();
    }
}