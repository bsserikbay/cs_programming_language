namespace MIInterfaceHierarchy
{
    public interface IShape: IDrawable, IPrintable
    {
     int GetNumberOfSides();   
    }
}