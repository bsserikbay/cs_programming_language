namespace MIInterfaceHierarchy
{
    public interface IDrawable
    {
        void Draw();
    }
}