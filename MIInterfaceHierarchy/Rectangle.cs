using System;

namespace MIInterfaceHierarchy
{
    public class Rectangle: IShape
    {
        public int GetNumberOfSides()
        {
            return 4;
        }

        public void Print()
        {
            Console.WriteLine("Printing ...");
        }

        void IPrintable.Draw()
        {
            Console.WriteLine("Drawing printer ...");
        }

        void IDrawable.Draw()
        {
            Console.WriteLine("Drawing ..");
        }
    }
}