﻿using System;

namespace SimpleClassExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*******  Fun with Class Types ********\n");
            //Разместить в памяти и конфигурировать:
            Car myCar = new Car();
            myCar.petName = "Henry";
            myCar.currSpeed = 20;

            for (int i = 0; i <= 10; i++)
            {
                myCar.SpeedUp(5);
                myCar.PrintState();
            }
            Console.ReadLine();
            
            Car chuck = new Car();
            chuck.PrintState();
            
            Car mary = new Car("Mary");
            mary.PrintState();

            Car daisy = new Car("Daisy", 76);
            daisy.PrintState();
            Console.ReadLine();

            Motorcycle mc = new Motorcycle(5);
            mc.SetDriverName("Tiny");
            mc.PopAWheely();
            Console.WriteLine("Rider name is {0}", mc.driverName);
            Console.ReadLine();

            Motorcycle2.MakeSomeBykes();
        }
             
    }
}