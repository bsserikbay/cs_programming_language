using System;

namespace SimpleClassExample
{
    public class Motorcycle2
    {
        public int driverIntensity;

        public string driverName;

        public Motorcycle2(int intensity = 0, string name = "")
        {
            Console.WriteLine("In master ctor");
            if (intensity > 10)
            {
                intensity = 10;
            }

            driverIntensity = intensity;
            driverName = name;
        }

        public static void MakeSomeBykes()
        {
            Motorcycle2 m2 = new Motorcycle2();
            Console.WriteLine("Name = {0}, Intensity = {1}", m2.driverName, m2.driverIntensity);

            Motorcycle2 m3 = new Motorcycle2(name: "Sonny");
            Console.WriteLine("Name = {0}, Intensity = {1}", m3.driverName, m3.driverIntensity);

            Motorcycle2 m4 = new Motorcycle2(7);
            Console.WriteLine("Name = {0}, Intensity = {1}", m4.driverName, m4.driverIntensity);
        }
    }
}