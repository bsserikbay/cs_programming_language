using System;

namespace SimpleClassExample
{
    public class Motorcycle
    {
        public int driverIntensity;
        
        public string driverName;

        public Motorcycle()
        {
            Console.WriteLine("In defauld ctor");
        }

        public Motorcycle(int intensity)
            : this(intensity, "")
        {
            Console.WriteLine("In ctor taking an int");
        }

        public Motorcycle(string name)
            : this(0, name)
        {
            Console.WriteLine("In ctor taking a string");
        }

        public Motorcycle(int intensity, string name)
        {
            Console.WriteLine("In master ctor");
            if (intensity > 10)
            {
                intensity = 10;
            }

            driverIntensity = intensity;
            driverName = name;
        }
        public void PopAWheely()
        {
            for (int i = 0; i < driverIntensity; i++)
            {
                Console.WriteLine("Yeeee Haaeewwww!");       
            }
        }
        public void SetDriverName(string name)
        {
            driverName = name;
        }

    }
}