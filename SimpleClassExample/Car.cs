using System;

namespace SimpleClassExample
{
    public class Car
    {
        //Состояние

        public string petName;
        public int currSpeed;

        public Car()
        {
            petName = "Chuck";
            currSpeed = 10;
        }

        public Car(string petName)
        {
            this.petName = petName;
        }

        public Car(string petName, int currSpeed)
        {
            this.petName = petName;
            this.currSpeed = currSpeed;
        }
        public void PrintState() 
        => Console.WriteLine("{0} is going {1} MPH.", petName, currSpeed);

        public void SpeedUp(int delta)
            => currSpeed += delta;
    }
}