﻿using System;

namespace CustomException
{
    class Program
    {
        static void Main(string[] args)
        {
            var myCar = new Car("Daisy2", 20);
            myCar.CrankTunes(true);

            try
            {
                    myCar.Accelerate(50);
                    Console.ReadLine();
            }
            catch (CarIsDeadException e)
            {
                Console.WriteLine("\n**** ERROR! *****");
                Console.WriteLine("Message: {0}", e.Message);
                Console.WriteLine(e.ErrorTimeStamp);
                Console.WriteLine(e.CauseOfError);
            }

            Console.ReadLine();
        }
    }
}