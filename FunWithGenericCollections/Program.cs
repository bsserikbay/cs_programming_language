﻿using System;
using System.Collections.Generic;

namespace FunWithGenericCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            UseGenericList();

        }
        static void UseGenericList()
        {
            List<Person> people = new List<Person>()
            {
                new Person {FirstName = "John", LastName = "Doe", Age = 47},
                new Person {FirstName = "Katty", LastName = "Poe", Age = 33},
                new Person {FirstName = "Bart", LastName = "Simpson", Age = 8}
            };
            
            Console.WriteLine("Items in list: {0}", people.Count);
            
            foreach (Person person in people)
                Console.WriteLine(person.FirstName + " : " + person.Age);
            
            Console.WriteLine("=>Interesting new person\n");
            people.Insert(2, new Person {FirstName="Maggie", LastName="Simpson", Age=2});
            Console.WriteLine("Items in list: {0}", people.Count);
            Person[] arrayOfPeople = people.ToArray();
            foreach (Person p in arrayOfPeople)
            {
                Console.WriteLine("First Names: {0}", p.FirstName);
            }
        }
    }
}