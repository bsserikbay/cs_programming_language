namespace CustomInterface
{
    public interface IDrawable
    {
        void Draw();
    }
}