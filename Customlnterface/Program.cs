﻿using System;
using CustomInterface;
using Shapes;

namespace Customlnterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with interfaces!");

            // Hexagon hex = new Hexagon();
            // Console.WriteLine("Points {0}", hex.Points);
            // Console.ReadLine();
            //
            // Hexagon hex2 = new Hexagon();
            // IPointy itfPt2 = hex2 as IPointy;
            // if (itfPt2 != null)
            // {
            //     Console.WriteLine("Points {0}", itfPt2.Points);
            // }
            // else
            // {
            //     Console.WriteLine("Not pointy");
            // }
            // Console.ReadLine();

            Shape[] myShapes = {new Hexagon(), new Circle("circle"), new Triangle("triangle")};
            for (int i = 0; i < myShapes.Length; i++)
            {
                myShapes[i].Draw();
                if (myShapes[i] is IPointy ip)
                {
                    Console.WriteLine("-> Points {0}", ip.Points);
                }

                if (myShapes[i] is IDraw3D)
                {
                    DrawIn3D((IDraw3D)myShapes[i]);
                }
                else
                {
                    Console.WriteLine("->{0}\' Not pointy!", myShapes[i].PetName);
                    Console.WriteLine();
                }
                Console.ReadLine();

                static void DrawIn3D(IDraw3D itd3d)
                {
                    Console.WriteLine("Drawing IDrow3D compatible types");
                    itd3d.Draw3D();
                }
                
                Console.WriteLine("***** Simple Interface Hierarchy ***★*");
                BitmapImage bitmap = new BitmapImage();
                bitmap.Draw();
                bitmap.DrawInBoundingBox(10, 10, 20, 20);
                bitmap.DrawUpsideDown();
                
                IAdvancedDraw iAdvDraw = bitmap as IAdvancedDraw;
                if (iAdvDraw != null)
                {
                    iAdvDraw.DrawUpsideDown();
                }
                Console.ReadLine();
                
            }
        }
    }
}