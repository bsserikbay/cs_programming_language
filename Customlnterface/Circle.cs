using System;

namespace Customlnterface
{
    public class Circle : Shape, IDraw3D
    {
        public Circle()
        {
            
        }

        public Circle(string name): base(name)
        {
            
        }
        
        public void Draw3D()
        {
            Console.WriteLine("Drawing Circle in 3D");
        }
    }
}