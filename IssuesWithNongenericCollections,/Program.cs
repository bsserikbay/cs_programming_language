﻿using System;
using System.Collections.Generic;

namespace IssuesWithNongenericCollections_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with Generics !");

        }

        static void UseGenerics()
        {
            List<Person> morePerson = new List<Person>();
            morePerson.Add(new Person("Frank", "Black", 50));
            Console.WriteLine(morePerson[0]);

            List<int> moreInts = new List<int>();
            moreInts.Add(19);
            moreInts.Add(2);
            int sum = moreInts[0] + moreInts[1];

        }

        static void SimpleBoxUnboxOperation()
        {
            int myInt = 25;

            object boxedInt = myInt;

            int unboxedInt = (int)boxedInt;

        }
    }
}
