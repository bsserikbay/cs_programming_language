﻿using System;

namespace SImpleDelegate
{
    class Program
    {
        public delegate int BinaryOp(int x, int y);

        public class SimpleMath
        {
            public  int Add(int x, int y) => x + y;
            public  int Sub(int x, int y) => x - y;
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine("Simple delegate example!");
            SimpleMath sm = new SimpleMath();

            BinaryOp b = new BinaryOp(sm.Add);
            DisplayDelegateInfo(b);
            Console.WriteLine("10 + 10 is {0}", b(10, 10));
            Console.ReadLine();
        }

        static void DisplayDelegateInfo(Delegate delObj)
        {
            foreach (Delegate d in delObj.GetInvocationList())
            {
                Console.WriteLine("Method name: {0}", d.Method);
                Console.WriteLine("Type name: {0}", d.Target);
            }
        }
    }

  

}