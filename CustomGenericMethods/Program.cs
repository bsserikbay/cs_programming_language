﻿using System;
using FunWithGenericCollections;

namespace CustomGenericMethods
{
    class Program
    {
        static void Main(string[] args)
        
        {
            Console.WriteLine("Fun with custom generic methods");
            int a =10, b=90;
            Console.WriteLine("Before swap: {0}, {1}", a, b) ; 
            Swap<int>(ref a, ref b);
            Console.WriteLine("After swap: {0}, {1}", a, b) ; 
            Console.WriteLine();
            
            string si = "Hello", s2 = "There"; 
            Console.WriteLine("Before swap: {0} {1}'", si, s2); 
            Swap<string> (ref si, ref s2); 
            Console.WriteLine("After swap: {0} {1}!", si, s2);
            Console.ReadLine();
            
            bool bl = true, b2 = false; 
            Console.WriteLine("Before swap: {0}, {1}", bl, b2) ; 
            Swap(ref bl, ref b2);
            Console.WriteLine("After swap: {0}, {1}", bl, b2);


        }
        static void Swap(ref int a, ref int b)
        {
            int temp = a;
            a = b;
            b = temp; 
        }

        static void Swap(ref Person a, ref Person b)
        {
            Person temp = a;
            a = b;
            b = temp;
        }
        
        static void Swap<T>(ref T a, ref T b)
        {
            Console.WriteLine("You sent the Swap() method a {0}", typeof(T)); 
                
            T temp = a;
            a = b;
            b = temp;
        }


    }
}