﻿using System;

namespace RefTypeValTypeParams
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("********    Passing Person object by value   *********");
            Person fred = new Person("Fred", 12);
            Console.WriteLine("\nBefore by value call, Person is: ");
            fred.Display();
            SendAPersonByValue(fred);
            Console.WriteLine("\nAfter by value call, Person is: ");
            fred.Display();
            Console.ReadLine();
            
            Console.WriteLine("********    Passing Person object by refference   *********");
            Person mel = new Person("Mel", 22);
            Console.WriteLine("\nBefore by value call, Person is: ");
            mel.Display();
            SendAPersonByRefference(ref mel);
            Console.WriteLine("\nAfter by value call, Person is: ");
            mel.Display();
            Console.ReadLine();
            

        }

      
        class Person
        {
            public string PersonName;
            public int PersonAge;

            public Person(string name, int age)
            {
                PersonName = name;
                PersonAge = age;
            }

            public Person() { }

            public void Display()
            {
                Console.WriteLine("Name: {0}, Age: {1}", PersonName, PersonAge);
            }
            
        }
        
        static void SendAPersonByValue(Person p)
        {
            p.PersonAge = 39;

            p = new Person("Nikki", 99);
        }
        
        static void SendAPersonByRefference(ref Person p)
        {
            p.PersonAge = 555;

            p = new Person("Nikki", 909);
        }
    }
}