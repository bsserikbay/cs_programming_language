using System;

namespace CarDelegateMethodGroupConversion
{
    public class Car
    {
        public int CurrentSpeed { get; set; } 
        public int MaxSpeed { get; set; } = 100; 
        public string PetName { get; set; }
        
        private bool carlsDead;
        public Car() {}

        public Car(string name, int maxSp, int currSp)
        {
            PetName = name;
            CurrentSpeed = currSp;
            MaxSpeed = maxSp;
        }
        public delegate void CarEngineHandler(string msgForCaller);
        private CarEngineHandler listOfHandlers;

        public void RegisterWithCarEngine(CarEngineHandler methodToCall)
        {
            
            listOfHandlers += methodToCall;
        }
        
        public void UnRegisterWithCarEngine(CarEngineHandler methodToCall)
        {
            listOfHandlers -= methodToCall; 
        }


        public void Accelerate(int delta)
        {
            if (carlsDead)
            {
                if (listOfHandlers != null)
                {
                    listOfHandlers("Sorry car is dead");
                }
            }
            else
            {
                CurrentSpeed += delta;
                if (10 == (MaxSpeed - CurrentSpeed) && listOfHandlers != null)
                {
                    listOfHandlers("Careful buddy, gonna blow!");
                }

                if (CurrentSpeed >= MaxSpeed)
                {
                    carlsDead = true;
                }
                else
                {
                    Console.WriteLine("CurrentSpeed = {0}", CurrentSpeed);
                }
            }
        }
    }
}