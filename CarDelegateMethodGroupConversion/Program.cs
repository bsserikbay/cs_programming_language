﻿using System;

namespace CarDelegateMethodGroupConversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Method Group Conversion");
            Car cl = new Car () ;
            cl.RegisterWithCarEngine(CallMeHere);
            Console.WriteLine("Speeding up!");
            for (int i = 0; i < 6; i++)
            {
                cl.Accelerate(20);
                Console.ReadLine() ;   
            }
            cl.UnRegisterWithCarEngine(CallMeHere);
            Console.WriteLine("Speeding up!");
            for (int i = 0; i < 6; i++)
            {
                cl.Accelerate(20);
                Console.ReadLine() ;   
            }

            static void CallMeHere(string msg)
            {
                Console.WriteLine("=> Message from Car: {0}", msg);
            }
        }
    }
}