﻿using System;
using SimpleClassExample;

namespace Garage
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            // Garage g = new Garage();
            // Console.WriteLine("NumberOfCars: {0}", g.NumberOfCars);
            // Console.WriteLine(g.MyAuto.petName);
            // Console.ReadLine();

            Car c = new Car();
            c.petName = "Carl";
            
            Garage g = new Garage();
            g.MyAuto = c;

            Console.WriteLine("NumberOfCars: {0}", g.NumberOfCars);
            Console.WriteLine("Your car name is {0}", g.MyAuto.petName);
            Console.ReadLine();

        }
    }
}