using SimpleClassExample;

namespace Garage
{
    public class Garage
    {
        public int NumberOfCars { get; set; }

        public Car MyAuto { get; set; }

        public Garage()
        {
            MyAuto = new Car();
            NumberOfCars = 1;
        }

        public Garage(Car car, int number)
        {
            MyAuto = car;
            NumberOfCars = number;
        }
            
    }
}