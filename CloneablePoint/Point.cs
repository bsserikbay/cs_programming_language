using System;

namespace CloneablePoint
{
    public class Point: ICloneable
    {
        public int X { get; set;}
        public int Y { get; set;}
        public PointDescription description = new PointDescription();

        public Point(int XPos, int YPos, string petName)
        {
            X = XPos;
            Y = YPos;
            description.PetName = petName;
        }

        public Point()
        {
            
        }
        
        //Переопределить Object.ToString()
        public override string ToString()
        {
            return $"X={X}; Y={Y}; Name = {description.PetName};\nID={description.PointId}\n";
        }

        public object Clone()
        {
         Point newPoint = (Point) this.MemberwiseClone();
         
         PointDescription currentDesc = new PointDescription();
         currentDesc.PetName = this.description.PetName;
         newPoint.description = currentDesc;
         return newPoint;
        }

    }
}