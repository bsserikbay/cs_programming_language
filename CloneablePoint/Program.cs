﻿using System;

namespace CloneablePoint
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("FUN WITH OBJECT CLONING!\n");
            Point p1 = new Point();
            Point p2 = p1;
            p2.X = 100;
            Console.WriteLine(p1);
            Console.WriteLine(p2);
            Console.ReadLine();
            
            Point p3 = new Point(100, 100, "Mary");
            Point p4 = (Point) p3.Clone();
            Console.WriteLine("Before modification");
            Console.WriteLine("p3: {0}", p3);
            Console.WriteLine("p4: {0}", p4);
            p4.description.PetName = "My new point";
            p4.X = 200;
            Console.WriteLine("\nChanged p.4.description.petName and p4.X");
            Console.WriteLine("After modification");
            Console.WriteLine("p3: {0}", p3);
            Console.WriteLine("p4: {0}", p4);
            Console.ReadLine();
        }
    }
}