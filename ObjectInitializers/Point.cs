using System;

namespace ObjectInitializers
{
    public enum PointColors
    {
        Blue,
        Red,
        Gold
    }
    
    
    public class Point
    {
        public int X { get; set;}
        public int Y { get; set;}
        
        public PointColors Color {get; set;}

        public Point(int xVal, int yVal)
        {
            X = xVal;
            Y = yVal;
            Color = PointColors.Red;
        }

        public Point(PointColors color)
        {
            Color = color;
        }

        public Point()
        : this(PointColors.Gold){ }

        public void DisplayStats()
        {
            Console.WriteLine("[{0}, {1}]", X, Y);
            Console.WriteLine("Point is {0}", Color);
        }
        
    }
    
}