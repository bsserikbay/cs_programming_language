﻿using System;

namespace ObjectInitializers
{
    class Program
    {
        static void Main(string[] args)
        {
            Point firstPoint = new Point();
            firstPoint.X = 10;
            firstPoint.Y = 20;
            firstPoint.DisplayStats();
            
            Point secondPoint = new Point(20, 31);
            secondPoint.DisplayStats();
            
            Point finalPoint = new Point{X = 11, Y = 44};
            finalPoint.DisplayStats();
            
            Point finalPoint2 = new Point() {X = 12, Y = 49};
            finalPoint2.DisplayStats();

            Point finalPoint3 = new Point(PointColors.Blue) {X = 900, Y = 900};
            finalPoint3.DisplayStats();

            Rectangle myRect = new Rectangle
            {
                TopLeft = new Point {X = 5, Y = 3},
                BottomRight = new Point {X = 90000, Y = 1111111}
            };
            
            myRect.DisplayStats();

        }
        
    }
}