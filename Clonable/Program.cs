﻿using System;

namespace Clonable
{
    class Program
    {
        static void Main(string[] args)
        {
            StringEquality();
            //Console.WriteLine();
            // Console.WriteLine("*** A first look at interfaces *** \n");

            // Эти классы поддерживают интерфейс ICloneable

            // string myStr = "Hello";
            // OperatingSystem unixOS = new OperatingSystem(PlatformID.Unix, new Version());
            // // System.Data.SqlClient.SqlConnection sqlCnn = new System.Data.SqlClient.SqlConnection();
            //
            // CloneMe(myStr);
            // CloneMe(unixOS);
            // Console.ReadLine();

        }

        public static void CloneMe(ICloneable c)
        {
            object theClone = c.Clone();
            Console.WriteLine("Your clone is a: {0}", theClone.GetType().Name);
        }


        static void StringEquality()
        {
            Console.WriteLine("=> String equality:");
            string si = "Hello!";
            string s2 = "Yo!";
            Console.WriteLine("si = {0}", si);
            Console.WriteLine("s2 = {0}", s2);
            Console.WriteLine();
            // Проверить строки на равенство.
            Console.WriteLine("si == s2: {0}", si == s2);
            Console.WriteLine("si==Hello!:{0}", si == "Hello!");
            Console.WriteLine("si==HELLO!:{0}", si == "HELLO!");
            Console.WriteLine("si == hello!: {0}", si == "hello!");
            Console.WriteLine("si.Equals(s2): {0}", si.Equals(s2));
            Console.WriteLine("Yo.Equals(s2): {0}", "Yo!".Equals(s2)); 
            Console.WriteLine();

        }
        

        
    }
}