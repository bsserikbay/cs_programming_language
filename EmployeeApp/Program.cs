﻿using System;

namespace EmployeeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("******   Fun with encapsulation! *******");
            // Employee emp = new Employee();
            // emp.GiveBonus(1000);
            // emp.DisplayStats();
            //
            // emp.SetName("Marv");
            // Console.WriteLine("Emplyee named {0}", emp.GetName());
            // Console.ReadLine();
            
            Employee joe = new Employee();
            for (int i = 0; i < 10; i++)
            {
                joe.Age++;    
            }
            
            Console.WriteLine("Joe age = {0}", joe.Age);

        }
    }
}