using System;

namespace EmployeeApp
{
    public class Employee
    {
        private int empAge;
        private string empName;
        private int empID;
        private float currPay;
        private string empSSN;

        public string SocialSecurityNumber
        {
            get { return empSSN; }

        }

        
        public int Age
        {
            get => empAge;
            set => empAge = value;
        }
        
        
        public Employee() { }

        public Employee(string name, int id, float pay)
        : this(name, 0, id, pay){}
        
        
        public Employee(string name, int age, int id, float pay)
        {
            empName = name;
            empID = id;
            empAge = age;
            currPay = pay;
        }
        
        public void DisplayStats()
        {
            Console.WriteLine("Name: {0}", empName);
            Console.WriteLine("ID: {0}", empID);
            Console.WriteLine("Age: {0}", empAge);
            Console.WriteLine("Pay: {0}", currPay);
        }



       

        private float Pay
        {
            get
            {
                return currPay;
            }
            set
            {
                currPay = value;
            }
        }

        public string Name
        {
            get { return empName;}
            set
            {
                if (value.Length > 15)
                {
                    Console.WriteLine("Error! Name must to be less than 15 characters");
                }
                else
                {
                    empName = value;
                }
            }
        }
        
        



        public string GetName()
        {
            
            return empName;
        }

        public void SetName(string name)
        {
            if (name.Length > 15)
            {
                Console.WriteLine("Error, name must be less than 15 characters");
            }
            empName = name;
        }

       

        public void GiveBonus(float amount)
        {
            currPay += amount;
        }

       

    }
}