﻿using System;
using System.Collections;

namespace CustomEnumerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with Enumerable/Enumerator\n");
            Garage carLot = new Garage();   

           IEnumerator i = carLot.GetEnumerator();
           i.MoveNext();
           Car car = (Car)i.Current;
           Console.WriteLine("{0} is going {1} MPH", car.PetName, car.CurrentSpeed);
           
           
          

        }
    }
}