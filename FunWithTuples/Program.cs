﻿using System;

namespace FunWithTuples
{
    class Program
    {
        static void Main(string[] args)
        {
            (string, int, string) values = ("a", 5, "c");

            values = ("a", 5, "c");

            Console.WriteLine($"First value = {values.Item1}");
            Console.WriteLine($"Second value = {values.Item2}");
            Console.WriteLine($"Third value = {values.Item3}");

            Console.ReadLine();
            
            Console.WriteLine("=> Inferred Tuple Names1");
            var foo = new {Prop1 = "first", Prop2 = "second"}; 
            var bar = (foo.Prop1, foo.Prop2); 
            Console.WriteLine($"{bar.Prop1};{bar.Prop2}");
            
            Console.ReadLine();

            var samples = FillTheseValues();
            Console.WriteLine($"Int is {samples.a}");
            Console.WriteLine($"String is {samples.b}");
            Console.WriteLine($"Bool is {samples.c}");
            
            Console.ReadLine();
            var userName = SplitName("John Doe");
            Console.WriteLine($"First name is {userName.firstName}");
            Console.WriteLine($"Second name is {userName.lastName}");

            Point p = new Point(9, 4);
            var pointValues = p.Deconstruct();
            Console.WriteLine($"X value = {pointValues.posX}");
            Console.WriteLine($"Y value = {pointValues.posY}");

        }

        static void FillTheseValues(out int a, out string b, out bool c)
        {
            a = 9;
            b = "Enjoy your string";
            c = true;
        }

        static (int a, string b, bool c) FillTheseValues()
        {
            return (9, "Enjoy your string", true);
        }
        
        static (string firstName, string lastName) SplitName(string name)
        {
            return ("John", "Doe");
        }

        struct Point
        {
            public int X;
            public int Y;

            public Point(int posX, int posY)
            {
                X = posX;
                Y = posY;
            }

            public (int posX, int posY) Deconstruct() => (X, Y);
        }
    }
}