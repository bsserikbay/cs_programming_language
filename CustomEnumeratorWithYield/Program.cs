﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CustomEnumeratorWithYield
{
    class Program
    {
        static void Main(string[] args)
        {
            Garage carLot = new Garage();
            foreach (Car c in carLot)
            {
                Console.WriteLine("{0} is going {1} MPH", c.PetName, c.CurrentSpeed);
            }

            Console.WriteLine();
            // IEnumerator carEnumerator = carLot.GetEnumerator () ;
            // Console.WriteLine(carEnumerator);

            foreach (Car c in carLot.GetTheCars(true))
            {
                Console.WriteLine("{0} is going {1} MPH", c.PetName, c.CurrentSpeed);

            }
            Console.WriteLine();       
        }
    }
}