using System;
using System.Collections;

namespace CustomEnumeratorWithYield
{
    public class Garage: IEnumerable
    {
        private Car[] carArray = new Car[4];

        public Garage()
        {
            carArray[0] = new Car("Rusty", 30);
            carArray[1] = new Car("Mike", 38);
            carArray[2] = new Car("Zippy", 41);
            carArray[3] = new Car("Carl", 30);
        }

        public IEnumerator GetEnumerator()
        {
            //throw new Exception("This won’t get called”);
            foreach (Car c in carArray)
            {
                yield return c;
            }
        }

        public IEnumerable GetTheCars(bool returnRevrsed)
        {
            return actualImplementation();

            IEnumerable actualImplementation()
            {
                if (returnRevrsed)
                {
                    for (int i = carArray.Length; i != 0; i--)
                    {
                        yield return carArray[i-1];
                    }
                }
                else
                {
                    foreach (var c in carArray)
                    {
                        yield return c;
                    }
                }
            }
        }
           
    }
}