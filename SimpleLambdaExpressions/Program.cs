﻿using System;
using System.Collections.Generic;

namespace SimpleLambdaExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with lambdas");
            TraditionalDelegateSyntax();
            AnonymousMethodSyntax();
            LambdaExpressionSyntax();

            static void TraditionalDelegateSyntax()
            {
                List<int> list = new List<int>();
                list.AddRange(new int[] {20, 1, 4, 8, 9, 44});

                Predicate<int> callback = isEvenNumber;
                List<int> evenNumbers = list.FindAll(callback);
                Console.WriteLine("Here are your even numbers");
                foreach (int evenNumber in evenNumbers)
                {
                    Console.Write("{0}\t", evenNumber);
                }

                Console.WriteLine();
            }

            static bool isEvenNumber(int i)
            {
                return (i % 2 == 0);
            }

            static void AnonymousMethodSyntax()
            {
                List<int> list = new List<int>();
                list.AddRange(new int[] {20, 1, 4, 8, 9, 44});

                List<int> evenNumbers = list.FindAll(delegate(int i) { return (i % 2) == 0; });
                Console.WriteLine("Here are your even numbers:");
                foreach (int evenNumber in evenNumbers)
                {
                    Console.Write("{0}\t", evenNumber);
                }

                Console.WriteLine();
            }


            static void LambdaExpressionSyntax()
            {
                List<int> list = new List<int>();
                list.AddRange(new int[] {20, 1, 4, 8, 9, 44});
                List<int> evenNumbers = list.FindAll(i => (i % 2) == 0);
                Console.WriteLine("Here are your even numbers:");
                foreach (int evenNumber in evenNumbers)
                {
                    Console.Write("{0}\t", evenNumber);
                }

                Console.WriteLine();
            }
        }

    }
}