﻿using System;

namespace ComparableCar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with object sorting!");

            Car[] myAutos = new Car[5];
            myAutos[0] = new Car("Rusty", 80, 1);
            myAutos[1] = new Car("Mary", 82, 129);
            myAutos[2] = new Car("Carl", 40, 12);
            myAutos[3] = new Car("Mel", 120, 3);
            myAutos[4] = new Car("Zippy", 90, 4);
            
            Console.ReadLine();

            Array.Sort(myAutos);
            Console.WriteLine();

            foreach (Car c in myAutos)
            {
                Console.WriteLine("{0} {1}", c.CarId, c.PetName);
            }
        }
    }
}