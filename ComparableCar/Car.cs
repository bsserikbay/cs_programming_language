using System;

namespace ComparableCar
{
    public class Car: IComparable
    {
        public int CarId { get; set; }
        public int CurrentSpeed;
        public string PetName { get; set; }

        public Car(string name, int currSpeed, int id)
        {
            CarId = id;
            CurrentSpeed = currSpeed;
            PetName = name;
        }

        int IComparable.CompareTo(object obj)
        {
            Car temp = obj as Car;
            if (temp != null)
            {
                if (this.CarId > temp.CarId)
                {
                    return 1;
                }

                if (this.CarId < temp.CarId)
                {
                    return -1;
                }
                else
                    return 0;
            }
            else
                throw new ArgumentException("Parameter is not a car");
        }
    }
}