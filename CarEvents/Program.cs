﻿using System;

namespace CarEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with Events");
            Car cl = new Car("SlugBug", 100, 10);

            cl.AboutToBlow += new Car.CarEngineHandler(CarlsAlmostDoomed);
            cl.AboutToBlow += new Car.CarEngineHandler(CarAboutToBlow);

            Car.CarEngineHandler d = new Car.CarEngineHandler(CarExploded);
            cl.Exploded += d;
            Console.WriteLine("***** Speeding up *****");
            for (int i = 0; i < 6; i++)
                cl.Accelerate(20);

            cl.Exploded -= d;
            Console.WriteLine("\n***** Speeding up *****");
            for (int i = 0; i < 6; i++)
                cl.Accelerate(20);
            Console.ReadLine();
            
            //Делегат с лямбда  выражением
            // Car car = new Car ("Zippy", 100, 10);
            // car.AboutToBlow += (sender, е) => { Console.WriteLine(е.msg);}; 
            // car.Exploded += (sender, e) => { Console.WriteLine(e.msg); };
        }

        public static void CarAboutToBlow(string msg)
        {
            Console.WriteLine(msg); 
                
        }
        public static void CarlsAlmostDoomed(string msg)
        { 
            Console.WriteLine("=> Critical Message from Car: {0}", msg); 
        }

        public static void CarExploded(string msg)
        {
            Console.WriteLine(msg); 
                
        }
    }
}