﻿using System;

namespace FunWithStructures
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** A First Look at Structures ***** \n");
            Point myPoint;
            myPoint.X = 349;
            myPoint.Y = 76;
            myPoint.Display();
            
            myPoint.Decrement();
            myPoint.Display();
            Console.ReadLine();

            
            Point p2;
            p2.X = 10;
            p2.Y = 10;
            p2.Display();

            Point p1 = new Point();
            p1.Display();
            
            Console.ReadLine();
            Point p3 = new Point(50, 60);
            p3.Display();
            

        }

        struct Point
        {
            public int X;
            public int Y;

            public Point(int XPos, int YPos)
            {
                X = XPos;
                Y = YPos;
            }

            public void Increment()
            {
                X++;
                Y++;
            }

            public void Decrement()
            {
                X--;
                Y--;
            }

            public void Display()
            {
                Console.WriteLine("X={0}, Y={1}", X, Y);
            }
        }
    }
}