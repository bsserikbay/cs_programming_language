﻿using System;
using System.Collections.ObjectModel;
using FunWithGenericCollections;

namespace FunWithObservableCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            ObservableCollection<Person> people = new ObservableCollection<Person>()
            {
                new Person {FirstName = "Peter", LastName = "Simpson", Age = 52},
                new Person {FirstName = "Nora", LastName = "Johnson", Age = 22}
            };
            people.CollectionChanged += people_CollectionChanged;
        }

        static void people_CollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Console.WriteLine("Action for this event: {0}",e.Action);
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                Console.WriteLine("Here are old elements: ");
                foreach (Person person in e.OldItems)
                {
                    Console.WriteLine(person.ToString());
                }

                Console.WriteLine();
            }

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                Console.WriteLine("Here are new elements: ");
                foreach (Person person in e.NewItems)
                {
                    Console.WriteLine(person.ToString());
                }
            }
        }
    }
}